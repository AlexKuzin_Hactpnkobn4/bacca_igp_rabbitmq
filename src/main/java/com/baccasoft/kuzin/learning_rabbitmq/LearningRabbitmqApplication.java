package com.baccasoft.kuzin.learning_rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearningRabbitmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearningRabbitmqApplication.class, args);
    }

}
