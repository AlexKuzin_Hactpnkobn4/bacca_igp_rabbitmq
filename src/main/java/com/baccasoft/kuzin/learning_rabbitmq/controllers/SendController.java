package com.baccasoft.kuzin.learning_rabbitmq.controllers;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static com.baccasoft.kuzin.learning_rabbitmq.utils.Constants.QUEUE_NAME;

@RestController
public class SendController {

    @GetMapping("/send")
    public String SendMessage(@RequestParam(defaultValue = "[EMPTY_MESSAGE]") String message) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            var response = String.format("Message '%s' was sent to queue '%s'.", message, QUEUE_NAME);

            System.out.println(response);
            return response;
        }
    }

}
