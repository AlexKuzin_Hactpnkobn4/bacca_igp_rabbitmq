package com.baccasoft.kuzin.learning_rabbitmq.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static com.baccasoft.kuzin.learning_rabbitmq.utils.Constants.QUEUE_NAME;

@RestController
public class RecieverController {

    List<String> messages = new ArrayList();

    @GetMapping("/receive")
    public String receiveMessage(){
        return "Messages: " + messages.stream().map(message -> "[" + message + "]").collect(Collectors.joining());
    }

    @PostConstruct
    public void RabbitMQListenerInit() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for RabitMQ messages for queue '" + QUEUE_NAME + "'.");
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            messages.add(message);
            System.out.println(" [x] Received '" + message + "'");
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }
}
